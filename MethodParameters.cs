﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEngine;
//[AddComponentMenu("")]
[System.Serializable]
public class MethodParameters : MonoBehaviour
{
    [SerializeField]
    public MethodList MethodValues = new MethodList();
    //private void Awake()
    //{
    //    MethodValues = new MethodList();
    //}

    [System.Serializable]
    public class MethodList
    {
        public MethodList()
        {
            Methods = new List<Method>();
        }

        [SerializeField]
        List<Method> Methods;
        public bool ContainsKey(MethodInfo key)
        {
            var met = Methods.Where(x => x.methodname == key.Name).ToList();
            if (met.Count > 0)
            {
                met[0].methodInfo = key;
                return true;
            }
            else return false;
        }

        public void Add(MethodInfo key, List<string> serializedValues)
        {
            Method m = new Method();
            m.methodname = key.Name;
            m.methodInfo = key;
            m.ParameterValues = serializedValues;
            Methods.Add(m);
        }
        public int Count
        {
            get
            {
                return Methods.Count;
            }
        }
        public List<string> this[MethodInfo key]
        {
            get
            {
                var a = Methods.Where(x => x.methodname == key.Name);
                var b = a.FirstOrDefault();
                var c = b.ParameterValues;
                return c;
            }
        }

    }
    [System.Serializable]
    public class Method
    {
        public Method()
        {
            ParameterValues = new List<string>();
        }
        [SerializeField]
        public string methodname;
        [SerializeField]
        public MethodInfo methodInfo;
        [SerializeField]
        public List<string> ParameterValues;
    }
}

