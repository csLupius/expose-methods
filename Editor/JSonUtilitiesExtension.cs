﻿using System;
using UnityEditor;
using UnityEngine;

namespace LUPI.EditorExtension
{
    internal static class JsonUtilityExtension
    {

        public static object ConvertFromJson<T>(this string self)
        {
            return self.ConvertFromJson(typeof(T));
        }
        public static object ConvertFromJson(this string self, Type type)
        {
            if (type == typeof(string))
            {
                return self;
            }
            else if (type == typeof(int))
            {
                return int.Parse(self);
            }
            else if (type == typeof(float))
            {
                return float.Parse(self);
            }
            else if (type == typeof(bool))
            {
                return bool.Parse(self);
            }
            else if (type == typeof(UnityEngine.Object) || typeof(UnityEngine.Object).IsAssignableFrom(type))
            {
                return (UnityEngine.Object)EditorUtility.InstanceIDToObject(int.Parse(self));
            }
            else
            {
                return JsonUtility.FromJson(self, type);
            }
        }
        public static string ConvertToJson<T>(this object self)
        {
            if (typeof(T) == typeof(string))
                return self.ToString();
            else if (typeof(T) == typeof(int))
                return self.ToString();
            else if (typeof(T) == typeof(float))
                return self.ToString();
            else if (typeof(T) == typeof(bool))
            {
                return self.ToString();
            }
            else if (typeof(T) == typeof(UnityEngine.Object) || typeof(UnityEngine.Object).IsAssignableFrom(typeof(T)))
            {
                return ((UnityEngine.Object)self).GetInstanceID().ToString();
            }
            else
            {
                return JsonUtility.ToJson(self);
            }
        }
    }
}