﻿using LUPI.EditorExtension;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEditor;
using UnityEngine;

//namespace LUPI.EditorExtensions
//{

    [CustomEditor(typeof(Component), true, isFallback = false)]
    [CanEditMultipleObjects]
    internal class ExposeMethodsAttributeEditor : UnityEditor.Editor
    {

        void GenerateParameterInput(MethodParameters values, MethodInfo method, ParameterInfo parameter)
        {
            if (values.MethodValues == null) values.MethodValues = new MethodParameters.MethodList();

            var pars = method.GetParameters();
            var parIndex = Array.IndexOf(pars, parameter);
            if (!values.MethodValues.ContainsKey(method))
            {
                values.MethodValues.Add(method, new List<string>());
            }
            string parFirstVal = "";
            if (parameter.ParameterType == typeof(string))
            {
                if (values.MethodValues[method].Count <= parIndex) values.MethodValues[method].Add(parameter.DefaultValue.ToString());
                parFirstVal = values.MethodValues[method][parIndex];

                values.MethodValues[method][parIndex] =
                    EditorGUILayout.TextField(
                        parameter.Name,
                            values.MethodValues[method][parIndex]);
            }
            else if (parameter.ParameterType == typeof(bool))
            {
                if (values.MethodValues[method].Count <= parIndex)
                    if (parameter.HasDefaultValue)
                        values.MethodValues[method].Add(parameter.DefaultValue.ConvertToJson<bool>());
                    else
                        values.MethodValues[method].Add(false.ConvertToJson<bool>());

                values.MethodValues[method][parIndex] =
                        EditorGUILayout.Toggle(
                            parameter.Name
                            , (bool)(values.MethodValues[method][parIndex]).ConvertFromJson<bool>()).ConvertToJson<bool>();
            }
            else if (parameter.ParameterType == typeof(Vector3))
            {
                if (values.MethodValues[method].Count <= parIndex)
                    if (parameter.HasDefaultValue)
                        values.MethodValues[method].Add(JsonUtility.ToJson((Vector3)parameter.DefaultValue));
                    else
                        values.MethodValues[method].Add(JsonUtility.ToJson(Vector3.zero));
                parFirstVal = values.MethodValues[method][parIndex];

                values.MethodValues[method][parIndex] =
                    JsonUtility.ToJson(
                        EditorGUILayout.Vector3Field(
                            parameter.Name
                            , JsonUtility.FromJson<Vector3>(values.MethodValues[method][parIndex])));
            }
            else if (parameter.ParameterType == typeof(Color))
            {
                if (values.MethodValues[method].Count <= parIndex)
                    if (parameter.HasDefaultValue)
                        values.MethodValues[method].Add(JsonUtility.ToJson((Color)parameter.DefaultValue));
                    else
                        values.MethodValues[method].Add(JsonUtility.ToJson(Color.white));
                parFirstVal = values.MethodValues[method][parIndex];

                values.MethodValues[method][parIndex] =
                    JsonUtility.ToJson(EditorGUILayout.ColorField(parameter.Name,
                    JsonUtility.FromJson<Color>(values.MethodValues[method][parIndex])));
            }
            else if (parameter.ParameterType == typeof(UnityEngine.Object) || typeof(UnityEngine.Object).IsAssignableFrom(parameter.ParameterType))
            {
                if (values.MethodValues[method].Count <= parIndex)
                    if (parameter.HasDefaultValue)
                        values.MethodValues[method].Add(((UnityEngine.Object)parameter.DefaultValue).GetInstanceID().ToString());
                    else
                        values.MethodValues[method].Add((-1).ToString());
                parFirstVal = values.MethodValues[method][parIndex];

                var memoryObject = (UnityEngine.Object)values.MethodValues[method][parIndex].ConvertFromJson<UnityEngine.Object>();

                var targetObject = EditorGUILayout.ObjectField(parameter.Name, memoryObject, parameter.ParameterType, true);

                if (targetObject != null)
                    values.MethodValues[method][parIndex] = targetObject.GetInstanceID().ToString();
            }
            else if (parameter.ParameterType == typeof(int))
            {
                if (values.MethodValues[method].Count <= parIndex)
                    if (parameter.HasDefaultValue)
                        values.MethodValues[method].Add(((int)parameter.DefaultValue).ToString());
                    else
                        values.MethodValues[method].Add((0).ToString());
                parFirstVal = values.MethodValues[method][parIndex];

                values.MethodValues[method][parIndex] =
                        EditorGUILayout.IntField(parameter.Name
                        , (int)
                        values.MethodValues[method][parIndex].ConvertFromJson<int>()).ToString();
            }
            else if (parameter.ParameterType == typeof(float))
            {
                if (values.MethodValues[method].Count <= parIndex)
                    if (parameter.HasDefaultValue)
                        values.MethodValues[method].Add(((float)parameter.DefaultValue).ToString());
                    else
                        values.MethodValues[method].Add((0f).ConvertToJson<float>());
                parFirstVal = values.MethodValues[method][parIndex];

                values.MethodValues[method][parIndex] =
                        EditorGUILayout.FloatField(parameter.Name
                        , (float)
                        values.MethodValues[method][parIndex].ConvertFromJson<float>()).ConvertToJson<float>();
            }
            else
            {
                Debug.LogWarning("Non-Supported Argument Type");
                return;
            }
            var parLastVal = values.MethodValues[method][parIndex];
            if (!object.Equals(parLastVal, parFirstVal))
            {
                EditorUtility.SetDirty(target);
            }

        }
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            var _targets = targets.OrderBy(x => x.name);
            foreach (var _target in _targets)
            {
                var targetType = _target.GetType();

                var component = (Component)target;
                bool anyObjectHasExposableAttribute =
                    component.GetComponents<Component>().Any(x => Attribute.GetCustomAttribute(x.GetType(), typeof(ExposableAttribute), true) != null);


                var attr = Attribute.GetCustomAttribute(targetType, typeof(ExposableAttribute), true);

                var metPars = component.gameObject.GetComponent<MethodParameters>();
                if (attr != null)
                {

                    var normalstyle = GUI.skin.label;
                    var whtstyle = normalstyle;
                    whtstyle.fontStyle = FontStyle.Bold;
                    whtstyle.normal.textColor = Color.white;
                    //EditorGUILayout.Space(25f);
                    var methods = targetType.GetMethods(BindingFlags.Instance | (BindingFlags.Public | BindingFlags.NonPublic));

                    foreach (var method in methods)
                    {
                        var metAttr = Attribute.GetCustomAttribute(method, typeof(ExposeAttribute), true);
                        if (metAttr == null) continue;

                        if (metPars == null)
                        {
                            metPars = component.gameObject.AddComponent<MethodParameters>();
                            metPars.hideFlags = HideFlags.HideInInspector;
                        }
                        else
                            metPars.hideFlags = HideFlags.HideInInspector;

                        var pars = method.GetParameters();
                        var methodname = System.Text.RegularExpressions.Regex.Replace(method.Name, "([a-z])([A-Z])", "$1 $2");

                        EditorGUILayout.LabelField(methodname);
                        GUIContent content = new GUIContent(methodname);
                        if (pars.Length > 0)
                            EditorGUILayout.BeginHorizontal(EditorStyles.helpBox);
                        else
                            EditorGUILayout.BeginHorizontal();
                        if (pars.Length > 0)
                        {
                            EditorGUILayout.BeginVertical();
                            float s = 0;
                            float t = 8f;
                            pars.ToList().ForEach(x =>
                            {
                                if (s < (float)x.Name.Length * t) s = (float)x.Name.Length * t;
                            });
                            EditorGUIUtility.labelWidth = s;
                            foreach (var par in pars)
                            {
                                GenerateParameterInput(metPars, method, par);
                            }
                            EditorGUILayout.EndVertical();
                        }
                        if (pars.Length > 0)
                            EditorGUILayout.BeginVertical(GUILayout.Width(25f));
                        else
                            EditorGUILayout.BeginVertical();

                        if (metAttr is RuntimeExpose)
                            GUI.enabled = Application.isPlaying;
                        else if (metAttr is EditorExpose)
                            GUI.enabled = !Application.isPlaying;

                        bool buttonClick = false;
                        GUIContent buttonContent = new GUIContent("Invoke");
                        buttonClick = GUILayout.Button(buttonContent);

                        if (buttonClick)
                        {
                            List<object> objArray = new List<object>();
                            foreach (var par in pars)
                            {
                                var parIndex = Array.IndexOf(pars, par);
                                objArray.Add(metPars.MethodValues[method][parIndex].ConvertFromJson(par.ParameterType));
                            }
                            method.Invoke(target, objArray.ToArray());
                        }
                        GUI.enabled = true;
                        EditorGUILayout.EndVertical();
                        EditorGUILayout.EndHorizontal();
                        EditorGUILayout.Space(5f);
                    }
                }
                else
                {

                    if (component.gameObject.activeInHierarchy)
                        if (!anyObjectHasExposableAttribute)
                        {
                            if (metPars != null)
                                DestroyImmediate(metPars);
                        }
                }

            }
        }
    }
//}


