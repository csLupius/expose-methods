﻿[System.AttributeUsage(System.AttributeTargets.Class, Inherited = true, AllowMultiple = true)]
public sealed class ExposableAttribute : System.Attribute
{
    public ExposableAttribute()
    {

    }
}