﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Exposable]
public class ExposedMethodsFailSample : MonoBehaviour
{
    [EditorExpose]
    public void ThisMethodIsExposedByCustomEditor()
    {
        Debug.Log("ThisMethodDoesnTShowInInspector");
    }

    [EditorExpose]
    public void ThisMethodDoesntShowAtAll()
    {
        Debug.Log("ThisMethodDoesntShowAtAll");

    }

}
