﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(ExposedMethodsFailSample))]
public class ExposedMethodsFailSampleEditor : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        GUI.backgroundColor = Color.gray;
        if (GUILayout.Button("This is a custom inspector button"))
        {
            ((ExposedMethodsFailSample)target).ThisMethodIsExposedByCustomEditor();
        }
    }
}
