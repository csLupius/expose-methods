﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Exposable]
public class ExposedMethodsSample : MonoBehaviour
{
    [EditorExpose]
    public void ExposedAllArgsMethod(
        string arg0
        , int arg1
        , float arg2
        , Vector3 arg3
        , GameObject arg4)
    {

    }
    [EditorExpose]
    public void ExposedMethod()
    {

    }
    [EditorExpose]
    public void ExposedStringMethod(string arg)
    {
        Debug.Log(arg);
    }
    [EditorExpose]
    public void ExposedVector3Method(Vector3 arg)
    {
        Debug.Log(arg);

    }
    [EditorExpose]
    public void ExposedIntMethod(int arg)
    {
        Debug.Log(arg);

    }
    [EditorExpose]
    public void ExposedGameObjectMethod(GameObject arg)
    {
        Debug.Log(arg);

    }
    [EditorExpose]
    public void ExposedFloatMethod(float arg)
    {
        Debug.Log(arg);

    }

    [EditorExpose]
    public void ExposedComponentMethod(ExposedMethodsFailSample arg)
    {
        Debug.Log(arg);
    }

    [Expose]
    public void ExposedBooleanMethod(bool arg)
    {

    }

    [EditorExpose]
    public void GenerateMap(int width, int height)
    {
        //Your Code.
    }

    [RuntimeExpose]
    public void PathFindToPoint(Vector3 target)
    {
        //Your Code
    }

}
